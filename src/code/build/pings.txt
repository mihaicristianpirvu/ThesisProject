User 1
Direct ping
rtt min/avg/max/mdev = 0.191/0.451/6.992/0.327 ms
rtt min/avg/max/mdev = 0.209/0.442/5.584/0.228 ms
rtt min/avg/max/mdev = 0.223/0.415/3.154/0.124 ms
rtt min/avg/max/mdev = 0.218/0.432/2.290/0.079 ms
rtt min/avg/max/mdev = 0.206/0.428/3.905/0.147 ms
rtt min/avg/max/mdev = 0.215/0.431/3.903/0.138 ms

TCP
rtt min/avg/max/mdev = 1.615/2.694/8.384/1.559 ms
rtt min/avg/max/mdev = 1.703/5.088/14.380/1.773 ms, pipe 2
rtt min/avg/max/mdev = 1.389/4.563/16.057/1.755 ms, pipe 2
rtt min/avg/max/mdev = 1.921/6.394/15.979/1.923 ms
rtt min/avg/max/mdev = 2.559/7.579/10.505/1.506 ms
rtt min/avg/max/mdev = 1.934/4.198/14.694/1.456 ms, pipe 2

UDP
rtt min/avg/max/mdev = 0.560/0.929/5.757/0.281 ms
rtt min/avg/max/mdev = 0.452/0.910/3.642/0.188 ms
rtt min/avg/max/mdev = 0.416/0.869/6.570/0.414 ms
rtt min/avg/max/mdev = 0.465/0.854/6.577/0.286 ms
rtt min/avg/max/mdev = 0.431/0.829/4.372/0.291 ms
rtt min/avg/max/mdev = 0.449/0.861/7.534/0.354 ms

User 2
Direct ping
rtt min/avg/max/mdev = 0.269/0.450/1.867/0.109 ms
rtt min/avg/max/mdev = 0.203/0.453/2.883/0.167 ms
rtt min/avg/max/mdev = 0.228/0.411/1.164/0.075 ms
rtt min/avg/max/mdev = 0.241/0.429/1.142/0.058 ms
rtt min/avg/max/mdev = 0.223/0.442/4.136/0.149 ms
rtt min/avg/max/mdev = 0.211/0.436/6.417/0.145 ms

TCP
rtt min/avg/max/mdev = 2.196/4.175/11.705/2.080 ms, pipe 2
rtt min/avg/max/mdev = 1.539/4.671/14.801/2.120 ms, pipe 2
rtt min/avg/max/mdev = 2.101/3.454/11.320/0.781 ms, pipe 2
rtt min/avg/max/mdev = 2.079/3.433/9.026/0.566 ms
rtt min/avg/max/mdev = 2.422/3.418/7.348/0.387 ms
rtt min/avg/max/mdev = 1.500/4.526/12.860/1.613 ms, pipe 2

UDP
rtt min/avg/max/mdev = 0.460/0.911/6.770/0.465 ms
rtt min/avg/max/mdev = 0.425/0.862/4.514/0.218 ms
rtt min/avg/max/mdev = 0.449/0.855/2.878/0.174 ms
rtt min/avg/max/mdev = 0.439/0.837/5.806/0.239 ms
rtt min/avg/max/mdev = 0.428/0.864/4.258/0.220 ms
rtt min/avg/max/mdev = 0.456/0.892/3.751/0.208 ms